package com.kolchak.controller;

public interface Controller {
    void printFieldWithAnnotation();

    void printAnnotationValue();

    void invokeThreeMethods();

    void setValueIntoField();
//
//    void myMethod(String a, int... args);
//
//    void myMethod(String... args);
}
