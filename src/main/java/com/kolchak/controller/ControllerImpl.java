package com.kolchak.controller;

import com.kolchak.annotation.MyField;
import com.kolchak.model.GenericModel;
import com.kolchak.model.Person;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller {
    Class clazz = Person.class;
    Person person = new Person();
//    GenericModel<Person> personGenericModel = new GenericModel<>(Person.class);

    @Override
    public void printFieldWithAnnotation() {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyField.class)) {
                System.out.println("Fields with annotation: " + field.getName());
            }
        }
    }

    @Override
    public void printAnnotationValue() {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyField.class)) {
                MyField myField = field.getAnnotation(MyField.class);
                System.out.println("Annotation value in class " + clazz.getSimpleName() + ": " + myField);
            }
        }
    }

    @Override
    public void invokeThreeMethods() {
            List<Method> methods = new ArrayList<>();
        try {
            methods.add(clazz.getDeclaredMethod("personBirthPlace"));
            methods.add(clazz.getDeclaredMethod("personAge"));
            methods.add(clazz.getDeclaredMethod("personSurname"));
            for (Method method : methods) {
                method.setAccessible(true);
                System.out.println(method.invoke(person));
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setValueIntoField() {
        try {
            Field birthPlace = clazz.getDeclaredField("birthPlace");
            birthPlace.setAccessible(true);
            if (birthPlace.getType() == String.class) {
                try {
                    birthPlace.set(person, "Ternopil`");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }


}
