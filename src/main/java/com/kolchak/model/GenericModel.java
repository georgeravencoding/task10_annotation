package com.kolchak.model;

import java.lang.reflect.InvocationTargetException;

public class GenericModel <T>{
    private T obj;

    private final Class<T> clazz;

    public GenericModel(Class<T> clazz) {
        this.clazz = clazz;
        try {
            obj = clazz.getConstructor().newInstance();

        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
