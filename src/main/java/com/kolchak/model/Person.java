package com.kolchak.model;

import com.kolchak.annotation.MyField;

public class Person {
    @MyField(weight = 100.1)
    private String name;
    @MyField
    private int age;
    private String birthPlace = "Lviv";

    private String personBirthPlace() {
        return birthPlace;
    }

    private int personAge() {
        return age;
    }

    private String personSurname() {
        return "Bond";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthPlace='" + birthPlace + '\'' +
                '}';
    }
}
